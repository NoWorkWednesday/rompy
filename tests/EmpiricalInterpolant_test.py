#!/usr/bin/python
import unittest
import rompy as rp
import numpy as np
import os
from time import localtime, asctime
import warnings
warnings.filterwarnings('error')

class ei_test(unittest.TestCase):

    def dimNReal(self, nIndependent, nDegenerate):

        # Make linearly independent data sets.
        t = np.linspace(0., 1., 100)
        inner = rp.Integration([t[0], t[-1]], num=len(t))
        dataSets = [np.cos(2.*np.pi*i*t) for i in range(nIndependent)]
        for i in range(len(dataSets)):
            dataSets[i] = inner.normalize(dataSets[i])

        # Add linearly degenerate data sets
        for i in range(nDegenerate):
            dsDegen = 0.*t
            for ds in dataSets:
                dsDegen += (2.*np.random.random() - 1.)*ds
            dsDegen = inner.normalize(dsDegen)
            dataSets.append(dsDegen)

        # Make a basis
        basis = rp.algorithms.ReducedBasis(inner, inner_type='real')
        basis.make(dataSets, 0, 1.e-6, num=len(dataSets))

        self.assertEqual(len(basis.basis), nIndependent)
        self.assertEqual(basis.basis.dtype, t.dtype)

        # Make the corresponding empirical interpolant
        ei = rp.algorithms.EmpiricalInterpolant(basis.basis)

        self.assertEqual(len(ei.B), nIndependent)
        self.assertEqual(ei.B.dtype, t.dtype)

        # Use the empirical interpolant on the 2 data sets
        maxErrs = [inner.Linfty(dd - ei.interpolate(dd)) for dd in dataSets]
        self.assertLess(max(maxErrs), 1.e-6)

    def dimN(self, nIndependent, nDegenerate):

        # Make linearly independent data sets.
        t = np.linspace(0., 1., 100)
        inner = rp.Integration([t[0], t[-1]], num=len(t))
        dataSets = [np.exp(1.j*2.*np.pi*i*t) for i in range(nIndependent)]
        for i in range(len(dataSets)):
            dataSets[i] = inner.normalize(dataSets[i])

        # Add linearly degenerate data sets
        for i in range(nDegenerate):
            dsDegen = 0.*1.j*t
            for ds in dataSets:
                dsDegen += (2.*np.random.random() - 1.)*ds
            dsDegen = inner.normalize(dsDegen)
            dataSets.append(dsDegen)

        # Make a basis
        basis = rp.algorithms.ReducedBasis(inner)
        basis.make(dataSets, 0, 1.e-6, num=len(dataSets))

        self.assertEqual(len(basis.basis), nIndependent)

        # Make the corresponding empirical interpolant
        ei = rp.algorithms.EmpiricalInterpolant(basis.basis)
        
        self.assertEqual(len(ei.B), nIndependent)

        # Use the empirical interpolant on the 2 data sets
        maxErrs = [inner.Linfty(dd-ei.interpolate(dd)) for dd in dataSets]
        self.assertLess(max(maxErrs), 1.e-6)

    def test_dim1(self):
        self.dimN(1, 0)

    def test_dim2(self):
        self.dimN(2, 0)

    def test_dim3(self):
        self.dimN(3, 0)

    def test_dim4(self):
        self.dimN(4, 0)

    def test_dim1_1degen(self):
        self.dimN(1, 1)

    def test_dim2_1degen(self):
        self.dimN(2, 1)

    def test_dim3_1degen(self):
        self.dimN(3, 1)

    def test_dim1_2degen(self):
        self.dimN(1, 2)

    def test_dim2_2degen(self):
        self.dimN(2, 2)

    def test_dim3_2degen(self):
        self.dimN(3, 2)

    def test_dim1_3degen(self):
        self.dimN(1, 3)

    def test_real(self):
        self.dimNReal(1, 0)
        self.dimNReal(2, 0)
        self.dimNReal(1, 1)
        self.dimNReal(2, 1)
        self.dimNReal(3, 0)
        self.dimNReal(3, 1)
        self.dimNReal(3, 2)

def main():
    unittest.main(exit=False)
    stopTime = str(asctime(localtime()))
    print "Finished {} at {}".format(os.path.basename(__file__), stopTime)

if __name__ == '__main__':
    main()

